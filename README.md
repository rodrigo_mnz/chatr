# ChatR #

ChatR is a chat app using the basics features of firebase. You can create a unique user, reset password, login and send messages.

### What is this repository for? ###

* Testing the basics features of Firebase.
* Version 1.0

### How do I get set up? ###

* git clone git@bitbucket.org:rodrigo_mnz/chatr.git
* import the project on android studio.
* ready to go.