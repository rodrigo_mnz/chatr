package com.rmcaetano.chatr.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.batch.android.Batch;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.rmcaetano.chatr.R;
import com.rmcaetano.chatr.api.BatchApi;
import com.rmcaetano.chatr.code.ChatAdapter;
import com.rmcaetano.chatr.code.FireUtil;
import com.rmcaetano.chatr.model.BatchMessage;
import com.rmcaetano.chatr.model.Message;
import com.rmcaetano.chatr.model.Recipients;

import java.util.ArrayList;
import java.util.Calendar;

public class ChatActivity extends BaseActivty {

    private RecyclerView recyclerView;
    private EditText edtUserInput;
    private Button btnSend;
    final Firebase fbMessages = FireUtil.getMessages();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        edtUserInput = (EditText) findViewById(R.id.edt_input_content);
        btnSend = (Button) findViewById(R.id.btn_input);

        Batch.User.getEditor()
                .setIdentifier(FireUtil.getLoggedUserData().getUid())
                .save();

        adjustTitle();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        setUpMessagesRecyclerView(fbMessages);
    }

    private void sendMessage() {
        final String content = edtUserInput.getText().toString().trim();
        if (content.length() > 0) {
            Message message = new Message(content,
                    fbMessages.getAuth().getUid(),
                    FireUtil.getLoggedUserName(),
                    Calendar.getInstance().toString());

            fbMessages.push().setValue(message);

            edtUserInput.setText("");

            final ArrayList<String> userIdsList = new ArrayList<>();

            //Send push
            FireUtil.getUsers().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        userIdsList.add(data.getKey());
                    }

                    String[] userIdsArray = new String[userIdsList.size()];
                    userIdsArray = userIdsList.toArray(userIdsArray);

                    sendBatchPush(userIdsArray, content);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }

    private void sendBatchPush(String[] userIdsArray, String content) {
        if (userIdsArray.length == 0) return;

        Recipients recipients = new Recipients(new String[0], userIdsArray);
        BatchMessage batchMessage = new BatchMessage("", FireUtil.getLoggedUserName() + ": " + content);

        BatchApi.sendBatchPush(recipients, batchMessage);
    }

    private void adjustTitle() {
        String titleFormat = "%s - %s";
        String newTitle = String.format(titleFormat, this.getTitle(), FireUtil.getLoggedUserName());
        this.setTitle(newTitle);
    }

    private void setUpMessagesRecyclerView(final Firebase fbMessages) {
        final ChatAdapter adapter = new ChatAdapter();

        fbMessages.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.addToDataList(dataSnapshot, s);

                recyclerView.scrollToPosition(0);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                adapter.replace(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.remove(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                adapter.moveItem(dataSnapshot, s);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("", "firebaseError: " + firebaseError.toString());
                //Do nothing
            }
        });

        recyclerView.setAdapter(adapter);
    }
}
