package com.rmcaetano.chatr.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.rmcaetano.chatr.R;
import com.rmcaetano.chatr.code.FireUtil;
import com.rmcaetano.chatr.model.User;

import java.util.Map;

/**
 * Created by rodrigo on 02/03/16.
 */
public class SignUpActivity extends BaseActivty {

    private EditText edtNick, edtEmail, edtPassword;
    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        edtNick = (EditText) findViewById(R.id.edt_nick);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnCreate = (Button) findViewById(R.id.btn_create);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyAllField()) {
                    showCreateConfirmation();
                }
            }
        });
    }

    private void showCreateConfirmation() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = getString(R.string.user_creation_confirmation);
        message = String.format(message, edtNick.getText().toString().trim());
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                verifyIfUserIsAvailable();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void verifyIfUserIsAvailable() {
        progressDialog.show();
        final String nick = edtNick.getText().toString().trim();
        final String email = edtEmail.getText().toString().trim();
        final String password = edtPassword.getText().toString();


        FireUtil.getUsers().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Object value = dataSnapshot.getValue();

                if (value != null && dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        final User user = data.getValue(User.class);
                        if (user.getNick().equalsIgnoreCase(nick)) {
                            showToast(getString(R.string.nickname_already_exists));
                            progressDialog.hide();
                            return;
                        }

                        if (user.getEmail().equalsIgnoreCase(email)) {
                            showToast(getString(R.string.email_already_exists));
                            progressDialog.hide();
                            return;
                        }
                    }
                }

                doCreateUser(email, password);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                progressDialog.hide();
            }
        });
    }

    private void doCreateUser(String email, String password) {
        FireUtil.getRoot().createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> result) {
                doAuthenticate();
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                progressDialog.hide();
                showToast(getString(R.string.error_message));
            }
        });
    }

    private void doAuthenticate() {
        FireUtil.getRoot().authWithPassword(edtEmail.getText().toString().trim(), edtPassword.getText().toString(), new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                progressDialog.hide();
                final String nick = edtNick.getText().toString().trim();

                FireUtil.setLoggedUserData(authData);
                FireUtil.setLoggedUserName(nick);
                FireUtil.getUsers().child(FireUtil.getLoggedUserData().getUid()).setValue(new User(nick, edtEmail.getText().toString().trim()));

                goToChat();
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                progressDialog.hide();
                showToast(getString(R.string.error_message));
            }
        });
    }

    private void goToChat() {
        Intent intent = new Intent(SignUpActivity.this, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean verifyAllField() {
        boolean canCreate = true;

        if (edtNick.getText().toString().trim().length() == 0) {
            edtNick.setError(getString(R.string.nickname_required));
            canCreate = false;
        }

        if (edtEmail.getText().toString().trim().length() == 0) {
            edtEmail.setError(getString(R.string.email_required));
            canCreate = false;
        }

        if (edtPassword.getText().toString().trim().length() == 0) {
            edtPassword.setError(getString(R.string.password_required));
            canCreate = false;
        }

        return canCreate;
    }
}
