package com.rmcaetano.chatr.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.rmcaetano.chatr.R;
import com.rmcaetano.chatr.code.FireUtil;
import com.rmcaetano.chatr.model.User;

/**
 * Created by rodrigo on 02/03/16.
 */
public class LoginActivity extends BaseActivty {

    private EditText edtEmail, edtPassword;
    private Button btnLogin, btnForgot, btnSignup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnForgot = (Button) findViewById(R.id.btn_forgot);
        btnSignup = (Button) findViewById(R.id.btn_signup);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFieldEmail() && checkFieldPassword()) {
                    loginWithFirebase();
                }
            }
        });

        btnForgot.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkFieldEmail()) {
                    showForgotPasswordConfirmation();
                }
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSignUp();
            }
        });
    }

    private void goToSignUp() {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    private void doForgotPassword() {
        progressDialog.show();
        FireUtil.getRoot().resetPassword(edtEmail.getText().toString().trim(), new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                progressDialog.hide();
                showToast(getString(R.string.password_reset_message));
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                progressDialog.hide();
                showToast(firebaseError.getMessage());
            }
        });
    }

    private void showForgotPasswordConfirmation() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = getString(R.string.reset_password);
        message = String.format(message, edtEmail.getText().toString().trim());
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                doForgotPassword();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void loginWithFirebase() {
        progressDialog.show();
        FireUtil.getRoot().authWithPassword(edtEmail.getText().toString().trim(), edtPassword.getText().toString(), new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                FireUtil.setLoggedUserData(authData);

                getUserNameFromServer(authData);
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                progressDialog.hide();
                showToast(firebaseError.getMessage());
            }
        });
    }

    private void getUserNameFromServer(AuthData authData) {
        FireUtil.getUsers().orderByKey().equalTo(authData.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.hide();

                String userName = null;
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    final User user = data.getValue(User.class);
                    userName = user.getNick();
                }

                if (userName != null) {
                    FireUtil.setLoggedUserName(userName);
                    goToChat();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                progressDialog.hide();
                showToast(getString(R.string.error_message));
            }
        });
    }

    private void goToChat() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean checkFieldEmail() {
        if (edtEmail.getText().toString().trim().length() == 0) {
            edtEmail.setError(getString(R.string.email_required));
        }
        return !(edtEmail.getText().toString().trim().length() == 0);
    }

    private boolean checkFieldPassword() {
        if (edtPassword.getText().toString().trim().length() == 0) {
            edtPassword.setError(getString(R.string.password_required));
        }

        return !(edtPassword.getText().toString().trim().length() == 0);
    }
}
