package com.rmcaetano.chatr.code;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

/**
 * Created by rodrigo on 02/03/16.
 */
public class FireUtil {

    public static final String FIREBASE_URL = "https://crosschatapp.firebaseio.com/";
    public static final String MESSAGES_PATH = "messages";
    public static final String USERS_PATH = "users";
    private static Firebase fbRoot;
    private static Firebase fbMessages;
    private static Firebase fbUsers;

    private static AuthData loggedUserData;
    private static String loggedUserName;

    public static String getLoggedUserName() {
        return loggedUserName;
    }

    public static void setLoggedUserName(String loggedUserName) {
        FireUtil.loggedUserName = loggedUserName;
    }

    public static AuthData getLoggedUserData() {
        return loggedUserData;
    }

    public static void setLoggedUserData(AuthData loggedUserData) {
        FireUtil.loggedUserData = loggedUserData;
    }

    public static Firebase getRoot() {
        if (fbRoot == null) {
            fbRoot = new Firebase(FIREBASE_URL);
        }

        return fbRoot;
    }

    public static Firebase getMessages() {
        if (fbMessages == null) {
            fbMessages = getRoot().child(MESSAGES_PATH);
        }

        return fbMessages;
    }

    public static Firebase getUsers() {
        if (fbUsers == null) {
            fbUsers = getRoot().child(USERS_PATH);
        }

        return fbUsers;
    }
}
