package com.rmcaetano.chatr.code;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.rmcaetano.chatr.R;
import com.rmcaetano.chatr.model.Message;

import java.util.ArrayList;

/**
 * Created by rodrigo on 07/03/16.
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Message> dataList;
    public static final int REMOTE = 1, LOCAL = 0;

    public ChatAdapter() {
        dataList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOCAL) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_local_item, parent, false);

            return new LocalMessageViewHolder(v);
        } else if (viewType == REMOTE) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_remote_item, parent, false);

            return new RemoteMessageViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Message message = dataList.get(position);
        if (getItemViewType(position) == LOCAL) {
            LocalMessageViewHolder localHolder = (LocalMessageViewHolder) holder;
            localHolder.author.setText(message.getAuthor());
            localHolder.content.setText(message.getContent());

        } else if (getItemViewType(position) == REMOTE) {
            RemoteMessageViewHolder remoteHolder = (RemoteMessageViewHolder) holder;
            remoteHolder.author.setText(message.getAuthor());
            remoteHolder.content.setText(message.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        final Message message = dataList.get(position);
        if (message.getUserid().equals(FireUtil.getRoot().getAuth().getUid())) {
            return LOCAL;
        }

        return REMOTE;
    }

    public void addToDataList(DataSnapshot dataSnapshot, String previousKey) {
        final Message message = dataSnapshot.getValue(Message.class);
        message.setKey(dataSnapshot.getKey());
        if (previousKey != null) {
            final int index = getIndex(previousKey);
            dataList.add(index, message);
            notifyItemInserted(index);
        } else {
            dataList.add(0, message);
            notifyItemInserted(0);
        }
    }

    public void replace(DataSnapshot dataSnapshot) {
        final Message newMessage = dataSnapshot.getValue(Message.class);
        newMessage.setKey(dataSnapshot.getKey());

        int index = getIndex(newMessage.getKey());

        dataList.set(index, newMessage);
        notifyItemChanged(index);
    }

    public void remove(DataSnapshot dataSnapshot) {
        int index = getIndex(dataSnapshot.getKey());
        dataList.remove(index);
        notifyItemRemoved(index);
    }

    public void moveItem(DataSnapshot dataSnapshot, String previousKey) {
        remove(dataSnapshot);
        addToDataList(dataSnapshot, previousKey);
    }

    private int getIndex(String messageKey) {
        int index = 0;
        for (Message message : dataList) {
            if (message.getKey().equals(messageKey)) {
                break;
            }
            index++;
        }
        return index;
    }

    public static class LocalMessageViewHolder extends RecyclerView.ViewHolder {

        TextView content, author;

        public LocalMessageViewHolder(View view) {
            super(view);

            content = (TextView) view.findViewById(R.id.txt_content);
            author = (TextView) view.findViewById(R.id.txt_author);
        }
    }


    public static class RemoteMessageViewHolder extends RecyclerView.ViewHolder {

        TextView content, author;

        public RemoteMessageViewHolder(View view) {
            super(view);

            content = (TextView) view.findViewById(R.id.txt_content);
            author = (TextView) view.findViewById(R.id.txt_author);
        }
    }
}
