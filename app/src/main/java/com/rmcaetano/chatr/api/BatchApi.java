package com.rmcaetano.chatr.api;

import android.content.Context;
import android.os.AsyncTask;

import com.rmcaetano.chatr.R;
import com.rmcaetano.chatr.model.BatchMessage;
import com.rmcaetano.chatr.model.BatchPush;
import com.rmcaetano.chatr.model.Recipients;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rodrigo on 16/03/16.
 */
public class BatchApi {

    private static final String API_BASE_URL = "https://api.batch.com";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit.Builder builder;
    private static Context context;

    public static void init(Context context) {
        BatchApi.context = context;
    }

    public static void sendBatchPush(Recipients recipients, BatchMessage batchMessage) {
        BatchPush batchPush = new BatchPush();
        batchPush.setRecipients(recipients);
        batchPush.setMessage(batchMessage);

        final BatchService batchService = BatchApi.createService(BatchService.class);
        final Call<ResponseBody> call = batchService.sendPush(context.getString(R.string.batch_api_key), batchPush);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final ResponseBody body = call.execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static <S> S createService(Class<S> serviceClass) {
        createBuilderIfNecessary();

        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    private static void createBuilderIfNecessary() {
        if (builder == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
            builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
        }
    }
}
