package com.rmcaetano.chatr.api;

import com.rmcaetano.chatr.model.BatchPush;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by rodrigo on 16/03/16.
 */
public interface BatchService {

    @Headers("Content-Type: application/json")
    @POST("/1.0/DEV56E839D44BD6329A853B8AB06D9/transactional/send")
    Call<ResponseBody> sendPush(@Header("X-Authorization") String authorization, @Body BatchPush batchPush);
}
