package com.rmcaetano.chatr.model;

/**
 * Created by rodrigo on 16/03/16.
 */
public class BatchMessage {

    public BatchMessage(String title, String body) {
        this.title = title;
        this.body = body;
    }

    String title;
    String body;
}
