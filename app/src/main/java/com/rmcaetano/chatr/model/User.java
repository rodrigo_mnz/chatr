package com.rmcaetano.chatr.model;

/**
 * Created by rodrigo on 02/03/16.
 */
public class User {
    String nick;
    String email;

    public User(String nick, String email) {
        this.nick = nick;
        this.email = email;
    }

    public User() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
