package com.rmcaetano.chatr.model;

/**
 * Created by rodrigo on 16/03/16.
 */
public class Recipients {

    public Recipients(String[] tokens, String[] custom_ids) {
        this.tokens = tokens;
        this.custom_ids = custom_ids;
    }

    String[] tokens;
    String[] custom_ids;
}
