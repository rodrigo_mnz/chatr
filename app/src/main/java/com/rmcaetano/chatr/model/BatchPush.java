package com.rmcaetano.chatr.model;

/**
 * Created by rodrigo on 16/03/16.
 */
public class BatchPush {

    String group_id = "Message";
    String push_time = "now";
    Recipients recipients;
    BatchMessage message;

    public void setRecipients(Recipients recipients) {
        this.recipients = recipients;
    }

    public void setMessage(BatchMessage message) {
        this.message = message;
    }
}
