package com.rmcaetano.chatr.model;

/**
 * Created by rodrigo on 02/03/16.
 */
public class Message {
    String content;
    String userid;
    String author;
    String timestamp;
    String key;

    public Message() {
    }

    public Message(String content, String userid, String author, String timestamp) {
        this.content = content;
        this.userid = userid;
        this.author = author;
        this.timestamp = timestamp;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
