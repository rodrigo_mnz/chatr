package com.rmcaetano.chatr;

import android.support.multidex.MultiDexApplication;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.firebase.client.Firebase;

/**
 * Created by rodrigo on 02/03/16.
 */
public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);
        Batch.Push.setGCMSenderId(getString(R.string.gcm_sender_id));
        Batch.setConfig(new Config(getString(R.string.batch_key)));
    }
}
